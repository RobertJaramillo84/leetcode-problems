/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */


class Solution {
 public:
  vector<int> inorderTraversal(TreeNode* root) 
  {
    //Make a vector to store our answers   
    vector<int> ans;
      
    //Create a stack so that we can build our answer as we go; the fact that a stack is LIFO makes this simple 
    stack<TreeNode*> stack;

    //While I have a root node or my stack is not empty 
    while (root || !stack.empty()) 
    {
    
      //Begin to process my root node 
      while (root)
      {
        //add my root node to my stack 
        stack.push(root);
         
        //Then reassign my root node to the left child 
        root = root->left;
      }
      //Set the root to the top of my stack now that we have traveresed it 
      root = stack.top();
      //Add the result to my answers 
      ans.push_back(root->val);

      //remove the top of my stack so the next element is added 
      stack.pop();
      //No since our left side has been processed we can move to the right side of our node 
      root = root->right;
    }

    return ans;
  }
};