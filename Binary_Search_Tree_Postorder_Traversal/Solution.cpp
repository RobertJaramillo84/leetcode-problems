/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> postorderTraversal(TreeNode* root) {
       
    //Now build a vector store 
    vector<int> ans; 

    //Check if I have a root   
    if (!root)
      return ans;
    
    
    
    //Make aq stack and add the root node to it 
    stack<TreeNode*> stack{{root}};

    //Since our stack isnt empty we can begin to process it 
    while (!stack.empty()) {
      //set the root to the top of our stack 
      root = stack.top();
      //remove the top of our stack 
      stack.pop();
      //push back the val to our answer 
      ans.push_back(root->val);
      //If I have a left store it in the stack  
      if (root->left)
        stack.push(root->left);
    //If I have a right store it in the stack 
      if (root->right)
        stack.push(root->right);
    }

    //Now that we have our answers from the top down we need to reverse it so that we start frim the bottom down
    reverse(begin(ans), end(ans));
    return ans;
    
};
};